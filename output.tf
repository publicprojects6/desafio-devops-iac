output "project_id" {
  value = module.gke.project_id
}

output "kubernetes_cluster_name" {
  value = module.gke.kubernetes_cluster_name
}

output "kubernetes_cluster_host" {
  value = module.gke.kubernetes_cluster_host
}

output "region" {
  value = module.vpc.region
}
