variable "project_id" {
  default     = "_PROJECT_ID_"
  type        = string
  description = "ID do projeto no GCP"
}

variable "organization_id" {
  default     = "_ORGANIZATION_ID_"
  type        = string
  description = "ID da Organização"
}

variable "region" {
  default     = "_REGION_"
  type        = string
  description = "Região onde será implementada a infra."
}

variable "zone" {
  default     = "_ZONE_"
  type        = string
  description = "Zona de disponibilidade onde será implementada a infra."
}

variable "gke_initial_node_count" {
  default = 1
  type    = number
}

variable "gke_version" {
  default     = "_GKE_VERSION_"
  type        = string
  description = "Versão do GKE que será implementada."
}

variable "gke_max_pods_per_node" {
  default = 110
  type    = number
}

variable "gke_node_selector" {
  default = "default"
  type    = string
}

variable "environment" {
  default     = "_CI_ENVIRONMENT_SLUG_"
  type        = string
  description = "Somente se environment = production, então o auto-scale de nodes será ativado"
}

variable "bucket_location" {
  default     = "_BUCKET_LOCATION_"
  type        = string
  description = "Localização do Bucket do tfstate"
}

variable "billing_account" {
  default     = "_BILLING_ACCOUNT_"
  type        = string
  description = "Conta de faturamento"
}
