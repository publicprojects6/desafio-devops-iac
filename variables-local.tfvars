variable "project_id" {
  default     = "fair-charmer-350812"
  type        = string
  description = "ID do projeto no GCP"
}

variable "organization_id" {
  default     = "462261226532"
  type        = string
  description = "ID da Organização"
}

variable "region" {
  default     = "us-east1"
  type        = string
  description = "Região onde será implementada a infra."
}

variable "zone" {
  default     = "us-east1-c"
  type        = string
  description = "Zona de disponibilidade onde será implementada a infra."
}

variable "gke_initial_node_count" {
  default = 1
  type    = number
}

variable "gke_version" {
  default     = "1.21.10-gke.2000"
  type        = string
  description = "Versão do GKE que será implementada."
}

variable "gke_max_pods_per_node" {
  default = 110
  type    = number
}

variable "gke_node_selector" {
  default = "default"
  type    = string
}

variable "environment" {
  default     = "staging"
  type        = string
  description = "Somente se environment = production, então o auto-scale de nodes será ativado"
}

variable "bucket_location" {
  default     = "US"
  type        = string
  description = "Localização do Bucket do tfstate"
}

variable "billing_account" {
  default     = "0133C7-1B6038-B2805F"
  type        = string
  description = "Conta de faturamento"
}
